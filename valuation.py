import pandas as pd

df = {'id':(1,2,3,4,5,6,7,8,9),
                  'price':(1000,1050,2000,1750,1400,7000,630,4000,1400),
                  'currency':('GBP','EU','PLN','EU','EU','PLN','GBP','EU','GBP'),
                  'quantity':(2,1,1,2,4,3,5,1,3),
                  'matching_id':(3,1,1,2,3,2,3,3,1)}
df = pd.DataFrame(df)

mtch = {'matching_id':(1,2,3),
        'top_priced_count':(2,2,3)}
mtch = pd.DataFrame(mtch)

rates = {'currency':('GBP','EU','PLN'),
         'ratio':(2.4,2.1,1)}
rates = pd.DataFrame(rates)

df['total_price'] = df['price']*df['quantity']
df['total_PLN'] = 0

for i in range(0, len(df['currency'])):
    for k in range(0, len(rates['currency'])):
        if df.iloc[i,2] == rates.iloc[k,0]:
            df.iloc[i,6] = df.iloc[i,5]*rates.iloc[k,1]

dta = ('matching_id', 'total_price', 'avg_price', 'currency', 'ignored_products_count')
dfdta = pd.DataFrame(columns = dta)

for i in range(0,len(mtch['matching_id'])):
    sub = df.loc[df['matching_id'] == mtch.iloc[i,0]]
    sub = sub.sort_values('total_PLN', ascending = False)
    pdcts = len(sub)
    itms = mtch.iloc[i,1]
    sub = sub.head(itms)
    nwdta = {'matching_id':mtch.iloc[i,0],
             'total_price':sum(sub.iloc[:,6]),
             'avg_price':sum(sub.iloc[:,6])/sum(sub.iloc[:,3]),
             'currency':'PLN',
             'ignored_products_count':pdcts - itms}
    dfdta = dfdta.append(nwdta, ignore_index=True)

dfdta.to_csv('result.csv', index=False)
