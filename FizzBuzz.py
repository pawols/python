print('Welcome to the FizzBuzz program!\n'
      'Provide your numbers to explore the excellent substituting potential of the FizBuzz!\n',
      'There are some rules:\n',
      ' 1) Number 1 has to be lower than number 2\n',
      ' 2) Numbers must be within 1 to 10000 range.\n',
      'Lets start!\n',
      sep = '')

print('Number 1:')
n = int(input())

print('Number 2:')
m = int(input())

if n >= m:
    print('Number 1 must be smaller than Number 2')
elif n < 1 or n > 10000:
    print('Number 1 is out of range')
elif m > 10000: #m must be greater than n and n must be within the range
    print('Number 2 is out of range')
else:
        print('\nResults:')
        for i in range(n,m+1):
            if  i % 3 == 0 and i % 5 == 0:
                print('FizzBuzz')
            elif i % 3 == 0:
                print('Fizz')
            elif i % 5 == 0:
                print('Buzz')
            else:
                print(i)